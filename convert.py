from Bio.PDB import MMCIFParser, PDBIO

def cifTopdb(cif_file,pdb_file):
    """ """
    p = MMCIFParser()
    struc = p.get_structure("", cif_file)
    io = PDBIO()
    io.set_structure(struc)
    io.save(pdb_file)

        
