import os
import tqdm
import json
import subprocess
import numpy as np
import urllib.request
import sequenceHandler as sh
import requests
import pandas as pd
from io import StringIO


def barUpdate(t):
    last_b = [0]
    def update_to(b=1, bsize=1, tsize=None):
        if tsize is not None:
            t.total = tsize
        t.update((b - last_b[0]) * bsize)
        last_b[0] = b
    return update_to

def getPFAM(targetFolder):
    """ """

    targetFolder=targetFolder if targetFolder[-1]=='/' else targetFolder+'/'
    # Dowload full PFAM sto file
    os.mkdir(targetFolder)
    with tqdm.tqdm(unit = 'B', unit_scale = True, unit_divisor = 1024, miniters = 1) as t:
        urllib.request.urlretrieve('ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.full.uniprot.gz',
                                    targetFolder+'PFAM.sto.gz',reporthook=barUpdate(t))
    #Uncompress
    subprocess.run('gunzip '+targetFolder+'PFAM.sto.gz',shell=True)
    # Split full sto file in individual family files and convert to fasta
    with open(targetFolder+'PFAM.sto','r',encoding="ISO-8859-1") as f:
        outF=open('__tmp','w')
        for l in tqdm.tqdm(f):
            outF.write(l)
            if l.rstrip()=="//":
                outF.close()
                os.rename('__tmp',famName+".sto")
                sh.stockholm2fasta(famName+".sto",targetFolder+famName+".fasta")
                os.remove(famName+".sto")
                outF=open('__tmp','w')
            if l[:7]=="#=GF AC":
                famName=l.split()[2].split('.')[0]
                print(famName)
    os.remove(targetFolder+'PFAM.sto')
    os.remove('__tmp')
    
def getUniprot(entry):
    """ """
    r = requests.get('https://rest.uniprot.org/uniprotkb/'+entry)
    data=json.loads(r.text)
    return data
    

def uniprotToEnsembl(uniprotID):
    """ """

def geneToEnsembl(gene):
    query= "https://rest.ensembl.org/xrefs/symbol/homo_sapiens/"+gene+"?content-type=application/json"
    r=requests.get(query)

    data = r.json()
    if len(data)>0:
        return data[0]['id']
    else:
        return NaN
    
def getEnsemblOrthologs(ensemblID,session=None, taxonomy=False):
    """ """
    server = "https://rest.ensembl.org/homology/id/"+ensemblID+"?aligned=0;sequence=protein"

    if session is None:
        session=requests.Session()
    r = session.get(server, headers={ "Content-Type" : "application/json"})
    if not r.ok:
        r.raise_for_status()
        sys.exit()

    orthologs=[]
    targetHit=()
    for ortho in  r.json()['data'][0]['homologies']:
        if ortho['type']=='ortholog_one2one':
            orthologs.append((ortho['target']['id']+"__"+ortho['target']['species'],
                              ortho['target']['seq']))
    targetHit=(ortho['source']['id']+"__"+ortho['source']['species'],ortho['source']['seq'])

    return [targetHit]+orthologs         
    
def getSpecies(uniprotIdList):
    """ Returns dataframe """
    if isinstance(uniprotIdList,str):
        uniprotIdList=[uniprotIdList]
    chunkSize=200
    ddf=pd.DataFrame()
    for chunk in range(0,len(uniprotIdList),chunkSize):
        query='%20OR%20'.join(uniprotIdList[chunk:chunk+chunkSize])
        r = requests.get('https://rest.uniprot.org/uniprotkb/search?format=tsv&fields=accession,id,organism_name&size=300&query=accession:'+query)
        df=pd.read_csv(StringIO(r.text),delimiter='\t')
        ddf=ddf.append(df)
    return ddf

def getLineage(uniprotIdList):
    """ Returns dataframe """
    if isinstance(uniprotIdList,str):
        uniprotIdList=[uniprotIdList]
    chunkSize=200
    ddf=pd.DataFrame()
    for chunk in range(0,len(uniprotIdList),chunkSize):
        query='%20OR%20'.join(uniprotIdList[chunk:chunk+chunkSize])
        r = requests.get('https://rest.uniprot.org/uniprotkb/search?format=tsv&fields=accession,id,lineage&size=300&query=accession:'+query)
        df=pd.read_csv(StringIO(r.text),delimiter='\t')
        taxa=df['Taxonomic lineage'].str.split(',',expand=True)
        df=pd.concat((df.drop(columns=['Taxonomic lineage']),taxa),axis=1)
        ddf=ddf.append(df)
    return ddf


def vep(rsIDlist):
    """

    """
    chunkSize=200

    server = "https://rest.ensembl.org"
    ext = "/vep/human/id"
    headers={ "Content-Type" : "application/json", "Accept" : "application/json"}
    s=requests.Session()
    ddf=pd.DataFrame()
    for chunk in tqdm.tqdm(range(0,len(rsIDlist),chunkSize)):
        rs=rsIDlist[chunk:chunk+chunkSize]
        data='{ "ids" : ['
        data+=','.join(['"'+rsID+'"' for rsID in rs])
        data+='] }'
        r = s.post(server+ext, headers=headers, data=data)
        df=pd.DataFrame.from_records(r.json())
        ddf=pd.concat((ddf,df))
    return ddf


def getGTEX(ensemblID):
    """ """

    latest=requests.get('https://rest.ensembl.org/archive/id/'+ensemblID+'?content-type=application/json').json()['latest']
    gencode=latest.split('.')

    query = 'https://gtexportal.org/rest/v1/expression/geneExpression?datasetId=gtex_v8&gencodeId='
    r=requests.get(query+gencode[0]+'.'+gencode[1]+'&format=json')
    data = r.json()
    while not data['geneExpression'] and int(gencode[1])>0:
        gencode[1]=str(int(gencode[1])-1)
        r=requests.get(query+gencode[0]+'.'+gencode[1]+'&format=json')
        data=r.json()

    return pd.DataFrame(data['geneExpression']).explode('data').astype({'data':float})

def getGeneVariants(gene):
    """ """
    ensemblID = geneToEnsembl(gene)
    ### Get variants from Ensembl
    server = "https://rest.ensembl.org/overlap/id/"+ensemblID+"?feature=variation"
    r = requests.get(server, headers={ "Content-Type" : "application/json"})
    if not r.ok:
        r.raise_for_status()
        sys.exit()

    ids = []
    consequences = []
    for snp in r.json():
        ids.append(snp['id'])
        consequences.append(snp['consequence_type'])

    df = pd.DataFrame({'id':ids,'consequences':consequences})

    r = requests.get("https://www.ncbi.nlm.nih.gov/snp/?term="+gene+"%5BGene%20Name%5D")
    print(r.text)
    return df

                      
import io
import requests
import Bio.SwissProt


def getSwissProt(entry):
    """ Dowload entry from Uniprot as Swissprot formatted text and return parsed object
        Useful (non-exhaustive) attributes of the returned object are
        e.sequence, e.gene_name, e.description, e.taxonomy, e.features, e.organelle
    """
    query=requests.get('https://www.uniprot.org/uniprot/'+entry+'.txt')
    f=io.StringIO(str(query.text))
    e=Bio.SwissProt.parse(f)
    for i in e:
        return i


def run_vep(request,grch37=False,request_session=False):
    """ Run online version of the Ensembl VEP predictor.
        Multiple request formats are accepted:
          - rsID: If the request string starts with rs
          - hgvs: {Chromosome}:g.{position}{refAllele}?{altAllele}
          - list: List of either rsIDs or hgvs (not mixed).
    
       Returns:
          
          - consequences (dict{request -> pd.DataFrame}) : Dictionnary mapping the request string(s) to
                                                           Dataframe(s) containing the mapped consequences
                                                           on all transcripts
    """

    chunk_size=100

    grch='grch37.' if grch37 else ''
    if request_session==False:
        request_session=requests.Session()
    headers={ "Content-Type" : "application/json", "Accept" : "application/json"}
    result={}
    if isinstance(request,list):
        for chunk in range(0,len(request),chunk_size):
            req=request[chunk:chunk+chunk_size]
            if req[0].startswith('rs'):
                data = '{ "ids" : ["'+'","'.join(req)+'"] }'
                r = request_session.post("https://"+grch+"rest.ensembl.org/vep/human/id",headers=headers,data=data)
            else:
                data = '{ "hgvs_notations" : ["'+'","'.join(req)+'"] }'
                r = request_session.post("https://"+grch+"rest.ensembl.org/vep/human/hgvs/",headers=headers,data=data)
            
            if r.ok:
                decoded = r.json()
            else:
                r.raise_for_status()
                
            res = {d['id']:pd.DataFrame(d['transcript_consequences']) for d in decoded}
            result.update(res)
    else:
        if request.startswith('rs'):
            r = request_session.get("https://"+grch+"rest.ensembl.org/vep/human/id/"+request,headers=headers)
        else:
            r = request_session.get("https://"+grch+"rest.ensembl.org/vep/human/hgvs/"+request,headers=headers)
            
        if r.ok:
            decoded = r.json()
        else:
            r.raise_for_status()

        result = {d['id']:pd.DataFrame(d['transcript_consequences']) for d in decoded}
            
    return result

